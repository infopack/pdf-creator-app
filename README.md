# PDF Creator App

This repo contains an small app which takes an infopack IRN as input and returns a pdf version.

## Usage

1. An IRN must be provided as a `GET` request to `/single?irn=<IRN>`
2. Infopack file must be of type `.partial.html`

## Build

```
docker build . -t registry.gitlab.com/infopack/pdf-creator-app:latest
docker push registry.gitlab.com/infopack/pdf-creator-app:latest
```

## Installation

### Docker

This app is packages as a docker image and all releases can be found [here](https://gitlab.com/infopack/pdf-creator-app/container_registry).

**Example**

```
docker run --rm --name pdf-creator-app -p 9000:9000 registry.gitlab.com/infopack/pdf-creator-app:<latest version>
```

### Helm

```
helm --namespace infopack-pdf-creator-app upgrade pdf-creator-app .
```