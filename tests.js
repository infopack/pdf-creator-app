import { InfopackCollectionExpander } from "./lib/collection.js";

import crypto from 'node:crypto'

function sha1(data) {
    return crypto.createHash('sha1').update(data).digest('hex');
}


let collection =
`{
    "name": "nrb-standard",
    "title": "Nationella riktlinjer",
    "version": "1.0.0",
    "content": [
    {
        "name": "intro",
        "title": "Introduktion",
        "contentType": "html",
        "content": "<h1>TEST</h1>"
    }, {
        "contentType": "subcollection",
        "name": "nrb-metoder",
        "title": "Metoder",
        "content": [{
            "contentType": "package",
            "irn": "swe-nrb:nrb-metoder:3.0.0"
        }]
    }, {
        "contentType": "subcollection",
        "name": "nrb-vardelistor",
        "title": "Värdelistor",
        "content": [{
            "contentType": "package",
            "irn": "swe-nrb:nrb-vardelistor:2.0.0"
        }]
    }]
}`

let diff = JSON.stringify(JSON.parse(collection))



async function run() {
    let collectionExpander = new InfopackCollectionExpander(JSON.parse(diff), {
        returnContent: false
    })

    let result = await collectionExpander.expand()
    console.log(collectionExpander.get())

    // console.log(collectionExpander.inputCollection)

   // console.log(collectionExpander.inputCollection.length, diff.length)
    //console.log(sha1(diff))
}

run()
