# FROM node:16.20.2 AS build
# WORKDIR /app
# COPY . .
# RUN npm install --only-production

FROM node:18.20.4-slim
WORKDIR /app
# COPY --from=build /app .

# We don't need the standalone Chromium
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true

RUN apt-get update
RUN apt-get install gnupg wget -y
RUN wget --quiet --output-document=- https://dl-ssl.google.com/linux/linux_signing_key.pub | gpg --dearmor > /etc/apt/trusted.gpg.d/google-archive.gpg
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update
RUN apt-get install google-chrome-stable -y --no-install-recommends
RUN rm -rf /var/lib/apt/lists/*

COPY . .
RUN npm install
EXPOSE 9000
CMD [ "npm", "start"]
