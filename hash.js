import { InfopackCollectionExpander } from "./lib/collection.js";

import crypto from 'node:crypto'

function sha1(data) {
    return crypto.createHash('sha1').update(data).digest('hex');
}


let collection =
`{
    "name": "nrb-standard",
    "title": "Nationella riktlinjer",
    "version": "1.0.0",
    "content": [
    {
        "contentType": "html",
        "content": "<h1>TEST</h1>"
    }, {
        "contentType": "subcollection",
        "name": "nrb-metoder",
        "title": "Metoder",
        "content": [{
            "contentType": "package",
            "irn": "swe-nrb:nrb-metoder:3.0.0"
        }]
    }, {
        "contentType": "subcollection",
        "name": "nrb-vardelistor",
        "title": "Värdelistor",
        "content": [{
            "contentType": "package",
            "irn": "swe-nrb:nrb-vardelistor:2.0.0"
        }]
    }]
}`

let diff = JSON.stringify(JSON.parse(collection))
let diff2 = JSON.stringify(JSON.parse(diff))

console.log(sha1(collection))
console.log(sha1(diff))
console.log(sha1(diff2))
