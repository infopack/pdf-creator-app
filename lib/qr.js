import QRCode from 'qrcode'

/**
 * Takes a url and returns a dataurl with the qrcode
 * @param {string} url 
 * @returns {Promise<string>}
 */
export async function renderQR(url) {
    return new Promise((resolve, reject) => {
        QRCode.toDataURL(url, function (err, urlString) {
            if(err) reject(err)
            resolve(urlString)
        })
    })
}
