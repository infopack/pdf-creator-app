export class NotFoundError extends Error {
    constructor(message) {
        super(message);
        this.name = 'NotFoundError';
    }
}

export class RunErrors extends Error {
    constructor(message, messages) {
        super(message);
        this.name = 'RunErrors';
        this.messages = messages
    }
}