import * as cheerio from 'cheerio'
import path from 'path'

function isUrl(string) {
    let urlObj
    try {
        let urlObj = new URL(string)
    } catch (error) {
        return false
    }
    return true
}

function isRelativeLink(string) {
    return !path.isAbsolute(string)
}

/**
 * 
 * @param {*} url 
 * @param {*} htmlString 
 * @returns 
 */
export function resolveRelativeLinks(url, htmlString) {
    const $ = cheerio.load(htmlString, {}, false)
    $('img').each((i, el) => {
        let relPath = $(el).attr('src')
        // make sure not to proceed if href attribute is missing
        if(!relPath) return
        if(!isUrl(relPath) && isRelativeLink(relPath)) {
            $(el).attr('src', path.join(path.dirname(url), relPath))
        }
    })
    $('a').each((i, el) => {
        let relPath = $(el).attr('href')
        // make sure not to proceed if href attribute is missing
        if(!relPath) return
        if(!isUrl(relPath) && isRelativeLink(relPath)) {
            $(el).attr('href', path.join(path.dirname(url), relPath))
        }
    })
    return $.html()
}