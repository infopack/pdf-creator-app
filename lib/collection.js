import micromatch from "micromatch"
import fetch from "node-fetch"
import { resolveRelativeLinks } from "./infopack-content.js"
import { HtmlValidate } from 'html-validate/node'
import { NotFoundError } from "./errors.js"
import packageMeta from "../package.json" assert { type: "json" }
import crypto from 'node:crypto'
import { getFormattedTimestamp } from "./utils.js"

const DEFAULT_REGISTRY = 'https://storage.googleapis.com/storage.infopack.io'

export function irnToUrl(irn) {
    let url = DEFAULT_REGISTRY

    let irnComponents = irn.split(':')

    irnComponents.forEach(component => {
        url = url + '/' + component
    })

    return url
}

/**
 * 
 * @param {string} url 
 * @param {string=[text|json]} type text or json 
 */
export async function fetchContent(url, type) {
    const res = await fetch(url)
    if(!res.ok) throw new NotFoundError()
    return type === 'text' ? await res.text() : await res.json()
}

async function getInfopackFileContent(url) {
    return await fetchContent(url, 'text')
}

async function getInfopackFileContentMeta(url) {
    return await fetchContent(url + '.meta.json', 'json')
}

/**
 * Takes a infopack url and extracts its content and metadata.
 * @param {text} url 
 * @returns 
 */
export async function infopackUrlResolver(url, returnContent = true) {
    let content = await getInfopackFileContent(url)
    let htmlValidate = new HtmlValidate()
    
    let tests = await htmlValidate.validateString(content)
    content = resolveRelativeLinks(url, content)
    let meta = await getInfopackFileContentMeta(url)

    let obj = {
        url,
        meta,
        valid: tests.valid,
        notValid: !tests.valid,
        messages: tests.results[0]?.messages || []
    }
    if(returnContent) {
        obj.content = content
    }
    return obj
}

/**
 * Expands infopack collections
 */
export class InfopackCollectionExpander {
    /**
     * @readonly
     */
    collection

    content

    #errors = []

    fetchedDate = getFormattedTimestamp()

    constructor(collection, options) {
        this.inputCollection = {...collection}
        this.collection = collection
        this.options = options || {}
        this.version = collection.version

        // set default props
        this.options.returnContent = this.options.hasOwnProperty('returnContent') ? (this.options.returnContent == 'true') : true
    }

    hasErrors = () => this.#errors.length > 0

    getErrors = () => this.#errors

    /**
     * @return {void}
     */
    expand = async () => {
        /**
         * TODO: Validate JSON file!
         */
        let contentEmbryos = []
        let groups = []
        /**
         * 
         * @param {*} content 
         */
        let crawler = async (coll, level, collectionPath = []) => {
            if(!coll.hasOwnProperty('contentType')) coll.contentType = 'subcollection'

            if (coll.contentType === 'subcollection') {

                collectionPath.push(coll.name)
                if(level === 1) {
                    collectionPath.push(coll.version)
                    collectionPath.push(this.fetchedDate)
                }
                
                groups.push({
                    collectionPath: collectionPath.join('/'),
                    name: coll.name,
                    title: coll.title
                })

                for await (const content of coll.content) {
                    level++
                    await crawler(content, level, [...collectionPath])
                }
            } else if (coll.contentType === 'file') {
                let url = irnToUrl(coll.irn)
                try {
                    let resolvedData = await this.infopackUrlResolver(url, this.options.returnContent)
                    const {
                        namespace,
                        version,
                        name
                    } = resolvedData.meta.meta
                    contentEmbryos.push({
                        kind: "infopack-file",
                        collectionPath: collectionPath.join('/'),
                        packagePath: [namespace, name, version].join('/'),
                        filePath: resolvedData.meta.path,
                        irn: coll.irn,
                        fetchedDate: this.fetchedDate,
                        ...resolvedData,
                        classes: coll.classes || undefined
                    })
                } catch (error) {
                    if(error instanceof SyntaxError) {
                        this.#errors.push({
                            errorType: 'SyntaxError',
                            message: 'Recieved non json-format',
                            url: url
                        })
                    } else if(error instanceof NotFoundError) {
                        this.#errors.push({
                            errorType: 'NotFoundError',
                            message: 'Url pointed to nowhere',
                            url: url
                        })
                    } else {
                        // we're lost!
                        throw error
                    }
                }
            } else if (coll.contentType === 'package') {

                let indexUrl = irnToUrl(coll.irn) + '/index.json'
                let indexObject = await fetchContent(indexUrl, 'json')
                let filePaths = indexObject.files.map(o => o.path)
                filePaths = filePaths.filter(p => p.endsWith('.partial.html'))

                // filter
                if (coll.filters) {
                    filePaths = micromatch(filePaths, coll.filters)
                }

                let tempContentEmbryos = []

                for await (const filePath of filePaths) {
                    let url = irnToUrl(coll.irn + '/' + filePath)
                    try {
                        let resolvedData = await this.infopackUrlResolver(url, this.options.returnContent)
                        
                        tempContentEmbryos.push({
                            kind: "infopack-file",
                            collectionPath: collectionPath.join('/'),
                            packagePath: coll.irn.split(':').join('/'),
                            filePath: filePath,
                            irn: coll.irn + ':' + filePath,
                            fetchedDate: this.fetchedDate,
                            ...resolvedData,
                            classes: coll.classes || undefined
                        })
                        
                    } catch (error) {
                        if(error instanceof SyntaxError) {
                            this.#errors.push({
                                errorType: 'SyntaxError',
                                message: 'Recieved non json-format',
                                url: url
                            })
                        }
                        console.log(error)
                    }
                }

                if(coll.sortByLabel) {
                    tempContentEmbryos.sort((a, b) => parseInt((a.meta.labels ?? {})[coll.sortByLabel] || 9999) - parseInt((b.meta.labels ?? {})[coll.sortByLabel] || 9999))
                }
                contentEmbryos = [...contentEmbryos, ...tempContentEmbryos]

            } else if (coll.contentType === 'html') {
                contentEmbryos.push({
                    kind: "html-file",
                    collectionPath: collectionPath.join('/'),
                    packagePath: '',
                    filePath: coll.name + '.partial.html',
                    content: coll.content,
                    meta: {
                        version: this.version,
                        title: coll.title,
                        description: coll.description
                    },
                    classes: coll.classes || undefined
                })
            }
        }

        await crawler(this.collection, 1)

        this.content = contentEmbryos
        this.groups = groups
    }

    get = () => {
        return {
            name: this.collection.name,
            title: this.collection.title,
            version: this.collection.version,
            content: this.content,
            groups: this.groups,
            $meta: {
                "expander": packageMeta.name,
                "version": packageMeta.version,
                "date": new Date(),
                "schemaHash": sha1(JSON.stringify(this.inputCollection)),
                "schema": this.inputCollection
            }
        }
    }



    infopackUrlResolver = infopackUrlResolver
}

function sha1(data) {
    return crypto.createHash('sha1').update(data).digest('hex');
}
