import puppeteer from "puppeteer"
import fs from 'fs'
import Handlebars from "handlebars"

import { InfopackCollectionExpander, infopackUrlResolver, irnToUrl } from "./collection.js"
import { renderQR } from "./qr.js"
import { RunErrors } from "./errors.js"

const standardTemplate = Handlebars.compile(fs.readFileSync('./templates/standard.template.html').toString())
const collectionTemplate = Handlebars.compile(fs.readFileSync('./templates/collection.template.html').toString())
const tocTemplate = Handlebars.compile(fs.readFileSync('./templates/partials/toc.template.html').toString())
const fileTemplate = Handlebars.compile(fs.readFileSync('./templates/partials/file.template.html').toString())

export async function createPdf(content) {
    const browser = await puppeteer.launch({ args: ['--no-sandbox'] })
    const page = await browser.newPage()
    await page.setContent(content, { waitUntil: 'networkidle2' })
    let pdfArray = await page.pdf({
        format: 'A4',
        displayHeaderFooter: true,
        headerTemplate: fs.readFileSync('./templates/header.template.html').toString(),
        footerTemplate: fs.readFileSync('./templates/footer.template.html').toString(),
        margin: {
            top: '80px',
            bottom: '80px',
            left: '60px',
            right: '60px'
        }
    })
    // Close browser.
    const pdfBuff = Buffer.from(pdfArray);
    await browser.close()
    return pdfBuff
}

export async function createPdfFromIrn(irn) {
    let url = irnToUrl(irn)
    let registryUrl = url.replace('https://storage.googleapis.com/storage.infopack.io/', 'https://registry.infopack.io/package/')
    let content = await infopackUrlResolver(url)
    let htmlContent = standardTemplate({...content, qrCode: await renderQR(registryUrl)})
    return await createPdf(htmlContent)
}

export async function createPdfFromCollection(collection, raw = false) {
    
    let coll = new InfopackCollectionExpander(collection)

    await coll.expand()

    if(coll.hasErrors()) {
        throw new RunErrors('run has errors', coll.getErrors())
    }

    let expandedCollection = await coll.get()

    let fileContents = expandedCollection.content.reduce((acc, curr) => {
        let fragment = fileTemplate(curr)
        return acc + fragment
    }, '')

    let contentString = collectionTemplate({
        name: collection.name,
        version: collection.version,
        toc: tocTemplate({ files: expandedCollection.content }),
        content: fileContents
    })

    if(raw) {
        return contentString
    } else {
        return await createPdf(contentString)
    }
    
}