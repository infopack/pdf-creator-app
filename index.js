import process from 'node:process'
import fs from 'node:fs'
import express from 'express'
import bodyParser from 'body-parser'
import slugify from 'slugify'
import { Ajv2020 } from 'ajv/dist/2020.js';
import cors from 'cors'

import {
    createPdfFromIrn,
    createPdfFromCollection,
} from './lib/create-pdf.js'
import { InfopackCollectionExpander } from './lib/collection.js'
import { RunErrors } from './lib/errors.js'

const ajv = new Ajv2020()
const schema = JSON.parse(fs.readFileSync('./schemas/collection.schema.json').toString())

const app = new express()

app.use(cors())

app.get('/', (req, res) => {
    let packageData = JSON.parse(fs.readFileSync('./package.json').toString())
    res.json({ message: 'Service OK', version: packageData.version })
})

app.get('/single', async (req, res) => {
    let test = req.query.irn.split(':')

    if (test.length !== 4) {
        return res.status(422).send('Bad IRN')
    }
    let filename = test[3].replace('/', '_')
    let buff
    try {
        buff = await createPdfFromIrn(req.query.irn)
    } catch (error) {
        console.log(error)
        return res.status(500).send('Something went wrong')
    }

    res.setHeader('Content-Type', 'application/pdf')
    res.setHeader('Access-Control-Expose-Headers', 'Content-Disposition')
    res.setHeader('Content-Disposition', `attachment; filename=${filename}.pdf`)
    res.send(buff)
})

app.use('/collection', bodyParser.json())

app.use('/collection', async (req, res, next) => {
    const valid = ajv.validate(schema, req.body)

    if(!valid) {
        res.status(422).json(ajv.errors)
    } else {
        next()
    }
})

app.post('/collection', async (req, res) => {
    let content = req.body
    const filename = slugify(content.name + '-' + content.version, { lower: true })

    try {
        let buff = await createPdfFromCollection(content)
        res.setHeader('Content-Type', 'application/pdf')
        res.setHeader('Content-Disposition', `attachment; filename="${filename}.pdf"`)
        res.setHeader('Access-Control-Expose-Headers', 'Content-Disposition')
        res.send(buff)
    } catch (error) {
        if(error instanceof RunErrors) {
            console.log(error.messages)
            return res.status(500).json(error.messages)
        } else {
            return res.status(500).send('Something went wrong')
        }
    }
})

app.post('/collection/html', async (req, res) => {
    let content = req.body

    try {
        let buff = await createPdfFromCollection(content, true)
        res.send(buff)
    } catch (error) {
        if(error instanceof RunErrors) {
            return res.status(500).json(error.messages)
        } else {
            return res.status(500).send('Something went wrong')
        }
    }
})

app.post('/collection/expanded', async (req, res) => {
    let content = req.body
    
    let coll = new InfopackCollectionExpander(content, req.query)

    await coll.expand()

    res.json(coll.get())
})

app.listen(9000, () => {
    console.log('app listening...')
})

let signals = ['SIGINT', 'SIGTERM', 'SIGQUIT'].forEach(signal => {
    process.on(signal, () => {
        console.info("Interrupted by: " + signal)
        process.exit()
    })
})
